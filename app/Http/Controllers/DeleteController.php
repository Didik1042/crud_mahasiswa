<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DeleteController extends Controller
{
    public function home() {
        $mahasiswa = DB::table('mahasiswa')->get();         // mengambil data dari table mahasiswa
        return view('home', ['mahasiswa' => $mahasiswa]);  // mengirim data mahasiswa ke view home
    }
    //delete data mahasiswa
    public function hapus($id) {
        
        DB::table('mahasiswa')->where('id', $id)->delete();
        return redirect('/');

    }

}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\CreateController::class, 'home']);
Route::get('/tambah', [App\Http\Controllers\CreateController::class, 'tambah']);
Route::post('/insert', [App\Http\Controllers\CreateController::class, 'insert']);
Route::get('/edit/{id}', [App\Http\Controllers\EditController::class, 'edit']);
Route::post('/update', [App\Http\Controllers\EditController::class, 'update']);
Route::get('/hapus/{id}', [App\Http\Controllers\DeleteController::class, 'hapus']);

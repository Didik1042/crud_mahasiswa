<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class EditController extends Controller
{
    public function home() {
        $mahasiswa = DB::table('mahasiswa')->get();         // mengambil data dari table mahasiswa
        return view('home', ['mahasiswa' => $mahasiswa]);  // mengirim data mahasiswa ke view home
    }
    //edit data mahasiswa
    public function edit($id) {
        
        $mahasiswa = DB::table('mahasiswa')->where('id', $id)->get();
        return view('edit', ['mahasiswa' => $mahasiswa]);
    }

    //update data mahasiswa
    public function update(Request $request) {
        DB::table('mahasiswa')->where('id', $request->id)->update([
            'nama_mahasiswa' => $request->nama,
            'nim_mahasiswa' => $request->nim,
            'kelas_mahasiswa' => $request->kelas,
            'prodi_mahasiswa' => $request->prodi,
            'fakultas_mahasiswa' => $request->fakultas
        ]);
        return redirect('/');
    }

}

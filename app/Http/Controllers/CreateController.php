<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CreateController extends Controller
{
    public function home() {
        $mahasiswa = DB::table('mahasiswa')->get();         // mengambil data dari table mahasiswa
        return view('home', ['mahasiswa' => $mahasiswa]);  // mengirim data mahasiswa ke view home
    }
    //menampilkan view form tambah mahasiswa
    public function tambah() {
        return view('tambah');
    }

    //insert data ke table mahasiswa
    public function insert(Request $request) {
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa' => $request->nama,
            'nim_mahasiswa' => $request->nim,
            'kelas_mahasiswa' => $request->kelas,
            'prodi_mahasiswa' => $request->prodi,
            'fakultas_mahasiswa' => $request->fakultas

        ]);
        return redirect('/');
    }

    
}
